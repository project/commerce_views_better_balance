<?php

/**
 * @file
 * Views definitions for commerce views better balance module.
 */

/**
 * Implements hook_views_data().
 */
function commerce_views_better_balance_views_data_alter(&$data) {
  $data['commerce_order']['commerce_order_better_balance'] = array(
    'title' => t('Order Better Balance'),
    'help' => t('A sortable and filterable order balance field.'),
    'field' => array(
      'handler' => 'commerce_views_better_balance_handler_better_balance',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'commerce_views_better_balance_handler_better_balance_sort',
    ),
    'filter' => array(
      'handler' => 'commerce_views_better_balance_handler_better_balance_filter',
    ),

  );
}
