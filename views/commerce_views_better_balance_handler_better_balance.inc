<?php
/**
 * @file
 * Definition of mymodule_handler_handlername.
 */

/**
 * Description of what my handler does.
 */
class commerce_views_better_balance_handler_better_balance extends views_handler_field {

  public static function better_balance_query($table) {
    return '(COALESCE((SELECT fdcot.commerce_order_total_amount FROM field_data_commerce_order_total AS fdcot WHERE fdcot.entity_id = ' . $table . '.order_id), 0)
      - COALESCE((SELECT SUM(cpt.amount) FROM commerce_payment_transaction AS cpt WHERE cpt.order_id = ' . $table . '.order_id AND cpt.status = \'success\'), 0)
    )';
  }


  function init(&$view, &$options) {
    parent::init($view, $options);
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['display_format'] = array('default' => 'formatted');

    return $options;
  }

  /**
   * Provide the currency format option.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['display_format'] = array(
      '#type' => 'select',
      '#title' => t('Display format'),
      '#options' => array(
        'formatted' => t('Currency formatted amount'),
        'raw' => t('Raw amount'),
      ),
      '#default_value' => $this->options['display_format'],
    );
  }

  function query() {
    $this->field_alias = 'commerce_order_better_balance';
    $this->ensure_my_table();
    $this->add_additional_fields();
    $this->query->add_field(NULL, self::better_balance_query($this->table_alias), 'commerce_order_better_balance');
  }

  /**
   * Provide the render element for display format.
   */
  public function render($values) {
    switch ($this->options['display_format']) {
      case 'formatted':
        $currency_format = isset($values->commerce_order_currency) ? $values->commerce_order_currency: commerce_default_currency();
        return commerce_currency_format($values->commerce_order_better_balance, $currency_format);

      case 'raw':
        return check_plain($values->commerce_order_better_balance);
    }
  }

}
