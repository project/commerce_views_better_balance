<?php
/**
 * @file
 * Definition of mymodule_handler_handlername.
 */

/**
 * Description of what my handler does.
 */
class commerce_views_better_balance_handler_better_balance_filter extends views_handler_filter_numeric {


  private $better_balance_query;


  function operators() {
    $operators = parent::operators();
    unset($operators['regular_expression']);
    unset($operators['empty']);
    unset($operators['not empty']);
    return $operators;
  }

  function query() {
    $this->ensure_my_table();
    $this->better_balance_query = commerce_views_better_balance_handler_better_balance::better_balance_query($this->table_alias);

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}('');
    }
  }


  function op_between($field) {
    if ($this->operator == 'between') {
      $this->query->add_where_expression(
        $this->options['group'],
        $this->better_balance_query . ' BETWEEN :min AND :max',
        array(
          ':min' => $this->convert_currency($this->value['min']),
          ':max' => $this->convert_currency($this->value['max']),
        )
      );
    }
    else {
      $this->query->add_where_expression(
        $this->options['group'],
        $this->better_balance_query . ' <= :min OR ' . $this->better_balance_query . ' >= :max',
        array(
          ':min' => $this->convert_currency($this->value['min']),
          ':max' => $this->convert_currency($this->value['max']),
        )
      );
    }
  }

  function op_simple($field) {
      $this->query->add_where_expression(
        $this->options['group'],
        $this->better_balance_query . ' ' . $this->operator . ' :value',
        array(
          ':value' => $this->convert_currency($this->value['value']),
        )
      );
  }

  private function convert_currency($value) {
    return $value * 100;
  }

}
