<?php
/**
 * @file
 * Definition of commerce_views_better_balance_handler_better_balance_sort.
 */

/**
 * Sort handler for order_better_balance.
 */
class commerce_views_better_balance_handler_better_balance_sort extends views_handler_sort {

  var $field_alias = 'commerce_order_better_balance';

  function option_definition() { 
    $options = parent::option_definition();

    return $options;
  }

  function options_form(&$form, & $form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Called to add the sort to a query.
   */
  function query() {
    $this->ensure_my_table();
    if(!isset($this->query->fields[$this->field_alias])){
      $this->better_balance_query = commerce_views_better_balance_handler_better_balance::better_balance_query($this->table_alias);
      $this->query->add_field(NULL, $this->better_balance_query, 'commerce_order_better_balance');
    }
    $this->query->add_orderby(NULL, NULL, $this->options['order'], $this->field_alias);
  }

}
